//
// Created by Jeroen Vandezande on 03/02/2021.
//

#ifndef OV5640__TYPES_H
#define OV5640__TYPES_H

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;

#endif //OV5640__TYPES_H
