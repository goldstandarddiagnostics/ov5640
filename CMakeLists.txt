cmake_minimum_required(VERSION 3.17)
project(OV5640 C)

set(CMAKE_C_STANDARD 11)

add_library(OV5640 ov5640.c types.h)